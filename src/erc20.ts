const Web3 = require('web3');

import { BN } from 'bn';
import { Registry, abi } from 'cic-client';
import Wallet from 'ethereumjs-wallet';
import { createTx } from './tx';
import { ReadParams, WriteParams } from './param';
import { sendAndPollFor } from './transport';

async function transfer(w3:any, wallet:Wallet, readParams:ReadParams, writeParams:WriteParams, amount:BN): Promise<object> {
	// prepare contract parameters
	const tokenContract = new w3.eth.Contract(abi['common']['erc20'], readParams.sourceTokenAddress);
	writeParams.txData = tokenContract.methods.transfer(
		writeParams.beneficiaryAddress, 
		Web3.utils.toBN(amount),
	).encodeABI();
	writeParams.targetAddress = readParams.sourceTokenAddress;

	const txRaw = await createTx(w3, wallet, readParams, writeParams);
	const txHashHex = Web3.utils.keccak256(txRaw);
	
	return {
		hash: txHashHex,
		raw: txRaw,
	}
}

/***
 * Executes a ERC20 transfer approval transaction pair.
 *
 * Two transactions are sent; an approval for 0 value, and a subsequent transaction for approval of the desired value.
 * 
 * @param w3 A connected Web3 instance
 * @param registry A registry with network contracts loaded
 * @param wallet An unlocked ethereumjs-wallet:Wallet object
 * @param readParams transaction parameters
 * @param writeParams transaction parameters
 * @return Promise returning the transaction hash of the final approval transaction as a hexadecimal string
 */
async function approveTransfer(w3:any, registry:Registry, wallet:Wallet, readParams:ReadParams, writeParams:WriteParams, amount:BN): Promise<Array<object>> {
	let txs = [];

	// prepare contract parameters
	const networkAddress = registry.contracts['bancor_network']._address;
	const tokenContract = new w3.eth.Contract(registry.abis['common']['erc20'], readParams.sourceTokenAddress);
	writeParams.gasHint = 50000; //targetAddress = readParams.sourceTokenAddress;
	writeParams.targetAddress = readParams.sourceTokenAddress;
	writeParams.txData = tokenContract.methods.approve(
		networkAddress,
		0,
	).encodeABI();
	let txRaw = await createTx(w3, wallet, readParams, writeParams);
	let txHashHex = Web3.utils.keccak256(txRaw);
	txs.push({
		hash: txHashHex,
		raw: txRaw,
	});

	writeParams.nonce++;
	writeParams.txData = tokenContract.methods.approve(
		networkAddress,
		amount,
	).encodeABI();
	txRaw = await createTx(w3, wallet, readParams, writeParams);
	txHashHex = Web3.utils.keccak256(txRaw);
	txs.push({
		hash: txHashHex,
		raw: txRaw,
	});
	writeParams.nonce++;

	return txs;
}


export {
	approveTransfer,
	transfer,
}
