const args = require('yargs');

/**
 * Command-line flags used in all entry point scripts
 *
 */
const standardArgs = args.option('config', {
	alias: 'c',
	type: 'string',
	description: 'absolute path to configuration file',

}).option('eth-provider', {
	alias: 'p',
	type: 'string',
	description: 'URL for JSON-RPC provider',

}).option('registry-address', {
	type: 'string',
	description: 'Ethereum address of registry contract',

});

export {
	standardArgs,
};
