import * as fs from 'fs';

import Wallet from 'ethereumjs-wallet';

const Web3 = require('web3');

async function unlockWallet(filename:string, passphrase:string): Promise<Wallet> {
	const keyfileJson = fs.readFileSync(filename, 'utf-8');
	const o = JSON.parse(keyfileJson);
	console.debug('unlocking wallet', o.address);
	const wallet = await Wallet.fromV3(keyfileJson, passphrase)
	return wallet;
}

function getWalletAddress(filename:string): string {
	const keyfileJson = fs.readFileSync(filename, 'utf-8');
	const o = JSON.parse(keyfileJson);
	return Web3.utils.toChecksumAddress(o['address']);
}

export {
	unlockWallet,
	getWalletAddress,
}
