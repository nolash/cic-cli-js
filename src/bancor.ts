const Web3 = require('web3');

import Wallet from 'ethereumjs-wallet';

import { BN } from 'bn';
import { Registry, abi } from 'cic-client';

import { approveTransfer } from '../src/erc20';
import { sendAndPollFor } from './transport';
import { createTx } from './tx';
import { zeroAddress } from './common';
import { ReadParams, WriteParams } from './param';


/***
 * Executes a conversion between two connecting tokens on a Bancor network.
 *
 * @param w3 A connected Web3 instance
 * @param registry A registry with network contracts loaded
 * @param wallet An unlocked ethereumjs-wallet:Wallet object
 * @param readParams transaction parameters
 * @param writeParams transaction parameters
 * @param amount source token value for the conversion
 * @param minReturn minimum value of destination token to accept after conversion (will revert the transaction if not met)
 * @return Promise returning the transaction hash of the conversion transaction as a hexadecimal string
 */
async function convert(w3:any, registry:Registry, wallet:Wallet, readParams:ReadParams, writeParams:WriteParams, amount:BN, minReturn:BN=1) {

	// allowance for the network contract to perform the conversion
	let txs = await approveTransfer(w3, registry, wallet, readParams, writeParams, amount);

	// prepare contract parameters
	if (writeParams.anchorTokenAddress === undefined)  { // token mint convert
		const tokenPath = [
			readParams.sourceTokenAddress,
			writeParams.destinationTokenAddress,
			writeParams.destinationTokenAddress,
		];

		writeParams.txData = registry.contracts['bancor_network'].methods.convert(
			tokenPath,
			amount,
			amount,
		).encodeABI();
	} else { // bonding curve convert
		const tokenPath = [
			readParams.sourceTokenAddress,
			readParams.sourceTokenAddress,
			writeParams.anchorTokenAddress,
			writeParams.destinationTokenAddress,
			writeParams.destinationTokenAddress,
		];
		writeParams.txData = registry.contracts['bancor_network'].methods.convert2(
			tokenPath,
			amount,
			minReturn,
			writeParams.feeAddress,
			writeParams.fee,
		).encodeABI();
	}

	const writeParamsTx = {
		...writeParams,
		nonce: writeParams.nonce++,
		gasHint: 400000,
		targetAddress: registry.contracts['bancor_network']._address,
	};
	const txRaw = await createTx(w3, wallet, readParams, writeParamsTx);
	const txHashHex = Web3.utils.keccak256(txRaw);
	txs.push({
		'hash': txHashHex,
		'raw': txRaw,
	});

	return txs;
}


/***
 * Wrapper for convert call needed to mint the initial tokens for a new converter on the Bancor network.
 *
 * @see convert
 */
async function mint(w3:any, registry:Registry, wallet:Wallet, readParams:ReadParams, writeParams:WriteParams, amount:number): Promise<Array<object>> {
	if (writeParams.anchorTokenAddress) {
		throw 'writeParams.anchorTokenAddress cannot be specified when minting';
	}
	return await convert(w3, registry, wallet, readParams, writeParams, amount); 
}

/***
 * Creates a new token and associated converter with readParams.sourceTokenAddress as reserve.
 *
 * @param w3 Connected Web3 object
 * @param registry A registry with network contracts loaded
 * @param wallet An unlocked ethereumjs-wallet:Wallet object
 * @param readParams transaction parameters
 * @param writeParams transaction parameters
 * @param amount source token value for the conversion
 */
async function createConverter(w3:any, registry:Registry, wallet:Wallet, readParams:ReadParams, writeParams:WriteParams): Promise<string> {
	const txData = registry.contracts['bancor_converter_registry'].methods.newConverter(
		0,
		writeParams.tokenName,
		writeParams.tokenSymbol,
		writeParams.tokenDecimals,
		writeParams.tokenConversionMaxFee,
		[
			readParams.sourceTokenAddress,
		],
		[
			writeParams.tokenReserveWeight,
		],
	).encodeABI();

	writeParams.txData = txData;

	const converterRegistryAddress = registry.contracts['bancor_converter_registry']._address;
	writeParams.targetAddress = converterRegistryAddress;
	const txRaw = await createTx(w3, wallet, readParams, writeParams); 

	const txHashHex = Web3.utils.keccak256(txRaw);
	const log = await sendAndPollFor(
		w3,
		txHashHex,
		txRaw,
		converterRegistryAddress,
		'0xf2e7cf6d6ed3f77039511409a43d4fa5108f09ab71d72b014380364c910233a5',
	); //.then(async (log) => {
	return w3.eth.abi.decodeParameter('address', log['topics'][1]);
	
}

async function acceptConverterOwnership(w3:any, registry:Registry, wallet:Wallet, readParams:ReadParams, writeParams:WriteParams): Promise<string> {
	const converterAddress = writeParams.targetAddress;
	console.log('converterAddress', converterAddress);
	const converter = new w3.eth.Contract(abi['bancor']['converter'], converterAddress);
	const txData = converter.methods.acceptOwnership(
	).encodeABI();

	writeParams.txData = txData;
	const txRaw = await createTx(w3, wallet, readParams, writeParams); 
	return w3.eth.sendSignedTransaction(txRaw);
}


async function setConverterFee(w3:any, registry:Registry, wallet:Wallet, readParams:ReadParams, writeParams:WriteParams): Promise<object> {
	const converter_metadata = registry.converters_t[readParams.sourceTokenAddress];
	const converterAddress = converter_metadata['address'];
	const converter = new w3.eth.Contract(abi['bancor']['converter'], converterAddress);

	const txData = converter.methods.setConversionFee(
		writeParams.fee,
	).encodeABI();

	writeParams.txData = txData;

	writeParams.targetAddress = converterAddress;
	const txRaw = await createTx(w3, wallet, readParams, writeParams); 

	const txHashHex = Web3.utils.keccak256(txRaw);
	const log = await sendAndPollFor(
		w3,
		txHashHex,
		txRaw,
		converterAddress,
		'0x81cd2ffb37dd237c0e4e2a3de5265fcf9deb43d3e7801e80db9f1ccfba7ee600', // ConversionFeeUpdate
	); //.then(async (log) => {
	const newFeeConfirmed = w3.eth.abi.decodeParameter('uint32', log['data'].substring(66));
	if (writeParams.fee != newFeeConfirmed) {
		throw 'New confirmed fee does not match requested fee';
	}
	const oldFee = w3.eth.abi.decodeParameter('uint32', log['data'].substring(2, 66));
	return {
		'old': oldFee,
		'new': newFeeConfirmed,
	};
}

export {
	acceptConverterOwnership,
	setConverterFee,
	createConverter,
	convert,
	mint,
}
