/***
 * Transaction confirmation polling loop with time delay
 *
 * @param w3 Connected Web3 object
 * @param txHash Transaction hash to poll status for, as hexademical string
 * @param contractAddress Contract address to match logs with, as hexadecimal string
 * @param topic Log topic to match, as hexadecimal string
 * @param successCallback Callback accepting a single argument with the matching log entry 
 * @param errorCallback Callback accepting a single argument with an error string
 * @todo configurable poll delay
 */
//function _pollFor(w3, txHash, contractAddress, topic, successCallback, errorCallback) {
function _pollFor(w3, txHash, successCallback, errorCallback, contractAddress?:string, topic?:string) {
	w3.eth.getTransactionReceipt(txHash).then((r) => {
		if (r !== null) {
			console.log('rcpt', r);
			if (!r.status) {
				errorCallback('tx fail', txHash);
				return;
			} else if (contractAddress === undefined) {
				successCallback(r);
				return;
			}
			for (let i = 0; i < r.logs.length; i++) {
				const log = r.logs[i];
				if (log.topics[0] == topic) {
					successCallback(log);
					return;
				}
			}
		}
		setTimeout(_pollFor, 1000, w3, txHash, successCallback, errorCallback, contractAddress, topic);
	});
}

/***
 * Entry point for transaction confirmation poll.
 *
 * @see _pollFor
 */
function pollFor(w3:any, txHash:string, contractAddress?:string, topic?:string) {
	return new Promise((whohoo, doh) => {
		_pollFor(w3, txHash, whohoo, doh, contractAddress, topic);
	});
}

/***
 * Sends a raw, signed transaction and starts transaction confirmation polling.
 *
 * @param w3 Connected Web3 object
 * @param txHash Transaction hash to poll status for, as hexademical string
 * @param txRaw Raw, signed transaction, as hexadecimal string
 * @param contractAddress Contract address to match logs with, as hexadecimal string
 * @param topic Log topic to match, as hexadecimal string
 */
function sendAndPollFor(w3:any, txHash:string, txRaw:string, contractAddress?:string, topic?:string) {
	return new Promise((whohoo, doh) => {
		w3.eth.sendSignedTransaction(txRaw).then((tx) => {;
			_pollFor(w3, txHash, whohoo, doh, contractAddress, topic);
		}).catch((e) => {
			doh(e);	
		});
	}).catch((e) => {
		console.error('send and poll fail ' + e);
	});
}


///***
// * Sends a raw, signed transaction and starts transaction confirmation polling.
// *
// * @param w3 Connected Web3 object
// * @param txHash Transaction hash to poll status for, as hexademical string
// * @param txRaw Raw, signed transaction, as hexadecimal string
// * @param contractAddress Contract address to match logs with, as hexadecimal string
// * @param topic Log topic to match, as hexadecimal string
// */
//function waitFor(w3, txHash, txRaw, contractAddress, topic) {
//	return new Promise((whohoo, doh) => {
//		w3.eth.subscribe('logs', {
//			'address': contractAddress, //registry.contracts['bancor_converter_registry']._address,
//		}, (e, r) => {
//			if (e != null) {
//				doh('cannot start subscription: ' + e);
//			}
//		}).on('data', (bh) => {
//			whohoo(bh);
//		}).on('error', (e) => {
//			console.error('subscriptionerrror');	
//		});
//	});
//	w3.eth.sendSignedTransaction(txRaw);
//};

export {
	sendAndPollFor,
//	waitFor,
	pollFor,
}
