import { Registry, abi } from 'cic-client';

/***
 * Convenience function for waiting for the full ready state of the registry.
 *
 * After the function returns, all data in the registry has been successfully loaded.
 *
 * @param registry Registry object to load
 * @param loadTokens If set to true, will load metadata for all tokens present in the network (default: false)
 * @return Promise returning the registry object
 */
function loadRegistry(registry:Registry, loadTokens:boolean=false): Promise<Registry> {
	const registryStateTarget = 3;
	return new Promise((whohoo, doh) => {
		let registryState = 0;
		registry.onregistryload = () => {
			registryState |= 1;
			if (registryState == registryStateTarget) {
				whohoo(registry);
			}
		};
		registry.ontokensload  = () => {
			registryState |= 2;
			if (registryState == registryStateTarget) {
				whohoo(registry);
			}
		};
		try {
			console.log('load', loadTokens);
			registry.load(loadTokens);
		} catch(e) {
			doh(e);
		}
	});
}

export {
	loadRegistry,
}
