const path = require('path');

import { Config } from 'confini';

const configTemplate = {
	bancor: {
		registry_address: "",
		conversion_fee: "0",
		conversion_fee_beneficiary: "",
	},
	eth: {
		provider: "wss://bloxberg-ws.dev.grassrootseconomics.net",
		chain_name: "bloxberg",
		chain_id: 8995,	
	},
	wallet: {
		keystore_file: "",
	},
}


/***
 * Convenience function to override configuration parameters by environment and command line args respectively
 *
 * @param configPath Path to directory containing configuration files in ini format
 * @param args dictionary of command line arguments applicable to configuration
 * @param argsTranslaction dictionary mapping keys in args to configuration keys
 * @param envs environment variable dictionary
 * @param envPrefix prefix to add to environment variable keys when matching
 *
 */
function loadConfig(configPath:string, args:object, argsTranslation:object, envs:object, envPrefix:string='', generate:boolean=true) {
	const c = new Config(configPath);

	if (generate) {
		c.generate(configTemplate);
	}
	c.process();

	// args
	let overrides = {};
	for (const k of Object.keys(argsTranslation)) {
		if (args[k] !== undefined) {
			const kk = argsTranslation[k];
			overrides[kk] = args[k];
		}
	}

	c.override(overrides)

	c.override(envs, envPrefix);

	return c;
}

export {
	loadConfig,
}
