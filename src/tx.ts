import { Transaction } from '@ethereumjs/tx';
import Wallet from 'ethereumjs-wallet';

const Web3 = require('web3');

import { ReadParams, WriteParams } from './param';

/***
 * Creates a signed Ethereum transaction using the ethereumjs
 * 
 * @param w3 A connected Web3 instance
 * @param wallet An unlocked ethereumjs-wallet:Wallet object
 * @param readParams transaction parameters
 * @param writeParams transaction parameters
 * @return Promise returning a signed transaction serialized to hexadecimal string
 */
async function createTx(w3:any, wallet:Wallet, readParams:ReadParams, writeParams:WriteParams): Promise<string> {
	// prepare tx parameters
	//const nonce = await w3.eth.getTransactionCount(wallet.getAddressString(), 'pending');
	let gasPrice = await w3.eth.getGasPrice();
	let maybeSafeGasLimit = 0;
	if (!writeParams.gasHint) { // false-ish is deliberate here
		const gasEstimate = await w3.eth.estimateGas({
			from: wallet.getAddressString(),
			to: writeParams.targetAddress,
			data: writeParams.txData,
		});
		maybeSafeGasLimit = Math.ceil(gasEstimate * 1.2);
	} else {
		maybeSafeGasLimit = writeParams.gasHint;
	}

	// build tx
	const txParams = {
		//nonce: Web3.utils.numberToHex(nonce),
		nonce: writeParams.nonce,
		gasLimit: Web3.utils.numberToHex(maybeSafeGasLimit),
		gasPrice: Web3.utils.numberToHex(gasPrice),
		data: writeParams.txData,
		to: writeParams.targetAddress,
		value: Web3.utils.numberToHex(0),
	};
	const tx = Transaction.fromTxData(txParams, {
		common: readParams.chainInfo,
	});
	const txSigned = tx.sign(wallet.getPrivateKey());
	const txRawBytes = txSigned.serialize();
	const txHash = w3.utils.keccak256(txRawBytes);
	console.debug('txhash', txHash, writeParams.nonce);
	return Web3.utils.bytesToHex(txRawBytes);
}

export {
	createTx,
}
