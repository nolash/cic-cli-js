import Common from '@ethereumjs/common';

/***
 * Contract parameters used for calls
 *
 */
interface ReadParams {
	chainInfo?: Common
	holderAddress: string
	sourceTokenAddress?: string
}

/***
 * Contract parameters used for transactions and calls
 *
 */
interface WriteParams {
	tokenName?: string,
	tokenSymbol?: string,
	tokenDecimals?: number,
	tokenConversionMaxFee?: number,
	tokenReserveWeight?: number,
	txData?: string,
	targetAddress?: string,
	beneficiaryAddress?: string,
	gasHint?: number,
	destinationTokenAddress?: string,
	anchorTokenAddress?: string,
	fee?: number,
	feeAddress?: string,
	nonce: number,
};

/***
 * Wallet creation parameters 
 *
 */
interface WalletParams {
	typ: string,
	provider: string,
	passphrase: string,
};

/***
 * Creates a ethereumjs/common:Common object for custom chain specifications with minimum required content
 *
 */
function newChainCommon(name:string, chainId:number, networkId?:number, args?:object) {
	if (networkId === undefined) {
		networkId = chainId;
	}
	return new Common({
		chain: {
			name: name,
			chainId: chainId,
			networkId: networkId,
			genesis: {},
			hardforks: [],
			bootstrapNodes: [],
		},
	});
}

export {
	ReadParams,
	WriteParams,
	newChainCommon,
	WalletParams,
}
