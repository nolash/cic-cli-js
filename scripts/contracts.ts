#!/usr/bin/env node

const { Console } = require('console');
console = new Console({
	stdout: process.stderr,
	stderr: process.stderr,
});

import * as path from 'path';

const args = require('yargs');

const Web3 = require('web3');

const xdgBasedir = require('xdg-basedir');

import Wallet from 'ethereumjs-wallet';
import { Transaction } from '@ethereumjs/tx';
import Common from '@ethereumjs/common';

import { Registry, abi } from 'cic-client';

import { standardArgs } from '../src/args';
import { loadRegistry } from '../src/registry';
import { loadConfig } from '../src/config';


let configPath = path.join(xdgBasedir.config, 'cic-cli');

const argv = standardArgs.usage(
	'ts-node contracts.ts [options]'
).argv;

if (argv['config'] !== undefined) {
	configPath = argv['config'];
}

const argvTranslation = {
	'keystore-file': 'WALLET_KEYSTORE_FILE',
	'registry-address': 'BANCOR_REGISTRY_ADDRESS',
	'provider': 'ETH_PROVIDER',
};
const config = loadConfig(configPath, argv, argvTranslation, process.env);
console.debug(config);


async function main(w3:any, registry:Registry) {
	await loadRegistry(registry, false);

	try {
		const c = await registry.getNetworkContracts();
		Object.keys(c).forEach((k) => {
			process.stdout.write(k + " " + c[k] + "\n");
		});
	} catch(e) {
		console.error('get contract list error ' + e);
	}

}

(async () => {
	let w3 = undefined;
	let r = 0;
	try {
		w3 = new Web3(config.get('ETH_PROVIDER'));
		const registry = new Registry(w3, config.get('BANCOR_REGISTRY_ADDRESS'), abi);

		await main(w3, registry);
	} catch(e) {
		r = 1;
		process.stderr.write('error: ' + e);
		try {
			w3.currentProvider.connection.close()
		} catch {
		}
	}

	try {
		w3.currentProvider.connection.close()
	} catch{
	}
	process.exit(r);
})();
