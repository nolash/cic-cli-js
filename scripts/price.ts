#!/usr/bin/env node

const { Console } = require('console');
console = new Console({
	stdout: process.stderr,
	stderr: process.stderr,
});

import * as path from 'path';

const Web3 = require('web3');

const xdgBasedir = require('xdg-basedir');

import Wallet from 'ethereumjs-wallet';

import { BN } from 'bn';

import { Transaction } from '@ethereumjs/tx';
import Common from '@ethereumjs/common';

import { Registry, abi } from 'cic-client';

import { standardArgs } from '../src/args';
import { convert } from '../src/bancor';
import { loadRegistry } from '../src/registry';
import { unlockWallet } from '../src/wallet/keystore';
import { getWalletAddress } from '../src/wallet/keystore';
import { zeroAddress } from '../src/common';
import { loadConfig } from '../src/config';
import { pollFor } from '../src/transport';
import { newChainCommon, ReadParams, WriteParams, WalletParams } from '../src/param';

let configPath = path.join(xdgBasedir.config, 'cic-cli');


const argv = standardArgs.usage(
	'ts-node price.ts [options] <source_token_amount>'
).option('source-token-address', {
	type: 'string',
	description: 'Source token address',

}).option('destination-token-address', {
	type: 'string',
	description: 'Destination token address',

}).argv;


// Input validation for args
if (argv['config'] !== undefined) {
	configPath = argv['config'];
}

const argvTranslation = {
	'registry-address': 'BANCOR_REGISTRY_ADDRESS',
	'provider': 'ETH_PROVIDER',
};
const config = loadConfig(configPath, argv, argvTranslation, process.env);
console.debug(config);



async function main(w3:any, registry:Registry, readParams:ReadParams, writeParams:WriteParams, amount:BN): Promise<string> {
	await loadRegistry(registry);

	const reserve = registry.reserves()[0];
	const reserveAddress = reserve._address;
	console.debug('reserve', reserveAddress);
	writeParams.anchorTokenAddress = reserveAddress;

	return registry.contracts['bancor_network'].methods.rateByPath(
		[
			readParams.sourceTokenAddress,
			readParams.sourceTokenAddress,
			writeParams.anchorTokenAddress,
			writeParams.destinationTokenAddress,
			writeParams.destinationTokenAddress,
		],
		amount,
	).call();

}


(async () => {
	let w3 = undefined;
	let r = 1;
	try {
		w3 = new Web3(config.get('ETH_PROVIDER'))
		const registry = new Registry(w3, config.get('BANCOR_REGISTRY_ADDRESS'), abi);
		
		const amount = Web3.utils.toBN(argv['_'][0]);

		const readParams = {
			sourceTokenAddress: Web3.utils.toChecksumAddress(argv['source-token-address']),
			holderAddress: undefined,
		}

		const writeParams = {
			destinationTokenAddress: Web3.utils.toChecksumAddress(argv['destination-token-address']),
			nonce: 0,
		}
	
		const result = await main(w3, registry, readParams, writeParams, amount); 
		process.stdout.write(result + '\n');

	} catch(e) {
		r = 1;
		process.stderr.write('error: ' + e + '\n');
		try {
			w3.currentProvider.connection.close()
		} catch {
		}
	}
	try {
		w3.currentProvider.connection.close()
	} catch {
	}
	process.exit(r);
})();
