#!/usr/bin/env node

const { Console } = require('console');
console = new Console({
	stdout: process.stderr,
	stderr: process.stderr,
});

import * as path from 'path';

const Web3 = require('web3');

const xdgBasedir = require('xdg-basedir');

import { Transaction } from '@ethereumjs/tx';

import { Registry, abi } from 'cic-client';

import { BN } from 'bn';
import { standardArgs } from '../src/args';
import { setConverterFee } from '../src/bancor';
import { loadRegistry } from '../src/registry';
import { getWalletAddress } from '../src/wallet/keystore';
import { unlockWallet } from '../src/wallet/keystore';
import { loadConfig } from '../src/config';
import { newChainCommon, ReadParams, WriteParams, WalletParams } from '../src/param';

let configPath = path.join(xdgBasedir.config, 'cic-cli');


const argv = standardArgs.usage(
	'ts-node token.ts [options] <fee-parts-per-million>'
).option('keystore-file', {
	alias: 'f',
	type: 'string',
	description: 'Keystore file of account to deploy token with',

}).option('token-address', {
	alias: 't',
	type: 'string',
	description: 'Token of converter to set fee on',

}).argv;


// Input validation for args
if (argv['config'] !== undefined) {
	configPath = argv['config'];
}
if (process.env['WALLET_PASSWORD'] === undefined) {
	throw 'please set the WALLET_PASSWORD environment variable to the keystore file password';
}

const argvTranslation = {
	'keystore-file': 'WALLET_KEYSTORE_FILE',
	'registry-address': 'BANCOR_REGISTRY_ADDRESS',
	'provider': 'ETH_PROVIDER',
};
const config = loadConfig(configPath, argv, argvTranslation, process.env);
console.debug(config);


async function main(w3:any, registry:Registry, walletParams:WalletParams, readParams:ReadParams, writeParams:WriteParams): Promise<string> {
	await loadRegistry(registry, true);

	const wallet = await unlockWallet(walletParams['provider'], walletParams['passphrase']);
	const walletAddress = wallet.getAddressString();
	writeParams.beneficiaryAddress = walletAddress;

	const fees = await setConverterFee(w3, registry, wallet, readParams, writeParams);

	return JSON.stringify(fees)
}


(async () => {
	let w3 = undefined;
	try {
		w3 = new Web3(config.get('ETH_PROVIDER'))
		const registry = new Registry(w3, config.get('BANCOR_REGISTRY_ADDRESS'), abi);

		const fee = Web3.utils.toBN(argv['_'][0]);

		const walletParams:WalletParams = {
			provider: config.get('WALLET_KEYSTORE_FILE'),
			typ: 'keystoreV3',
			passphrase: process.env['WALLET_PASSWORD'],
		}
		const walletAddress = getWalletAddress(config.get('WALLET_KEYSTORE_FILE'));

		const common = newChainCommon(config.get('ETH_CHAIN_NAME'), parseInt(config.get('ETH_CHAIN_ID'), 10));	
		const readParams:ReadParams = {
			holderAddress: getWalletAddress(config.get('WALLET_KEYSTORE_FILE')),
			sourceTokenAddress: argv['token-address'],
			chainInfo: common,
		};

		const nonce = await w3.eth.getTransactionCount(walletAddress, 'pending');
		const writeParams:WriteParams = {
			fee: fee,
			nonce: nonce,
		}

		const result = await main(w3, registry, walletParams, readParams, writeParams);
		process.stdout.write(result + '\n');
		try {
			w3.currentProvider.connection.close()
		} catch {
		}
	} catch(e) {
		process.stderr.write('error: ' + e);
		try {
			w3.currentProvider.connection.close()
		} catch {
		}
		process.exit(1);
	}
})();
