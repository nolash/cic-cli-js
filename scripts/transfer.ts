#!/usr/bin/env node

const { Console } = require('console');
console = new Console({
	stdout: process.stderr,
	stderr: process.stderr,
});

import * as path from 'path';

const args = require('yargs');

const Web3 = require('web3');

const xdgBasedir = require('xdg-basedir');

import Wallet from 'ethereumjs-wallet';

import { BN } from 'bn';
import { Transaction } from '@ethereumjs/tx';
import Common from '@ethereumjs/common';

//import { Registry, abi } from 'cic-client';
import { abi } from 'cic-client';

import { unlockWallet } from '../src/wallet/keystore';
import { getWalletAddress } from '../src/wallet/keystore';
import { loadConfig } from '../src/config';
import { transfer } from '../src/erc20';
import { newChainCommon, ReadParams, WriteParams, WalletParams } from '../src/param';

let configPath = path.join(xdgBasedir.config, 'cic-cli');

const argv = args.usage(
	'ts-node transfer.ts [options] <amount>'

).option('config', {
	alias: 'c',
	type: 'string',
	description: 'absolute path to configuration file',

}).option('eth-provider', {
	alias: 'p',
	type: 'string',
	description: 'URL for JSON-RPC provider',

}).option('keystore-file', {
	alias: 'f',
	type: 'string',
	description: 'Keystore file of account to deploy token with',

}).option('token-address', {
	alias: 't',
	type: 'string',
	description: 'Token address',

}).option('recipient-address', {
	alias: 'r',
	type: 'string',
	description: 'Recipient address',

}).argv;

// Input validation for args
if (argv['config'] !== undefined) {
	configPath = argv['config'];
}
if (process.env['WALLET_PASSWORD'] === undefined) {
	throw 'please set the WALLET_PASSWORD environment variable to the keystore file password';
}

const argvTranslation = {
	'keystore-file': 'WALLET_KEYSTORE_FILE',
	'provider': 'ETH_PROVIDER',
};
const config = loadConfig(configPath, argv, argvTranslation, process.env);
console.debug(config);


async function main(w3:any, walletParams:WalletParams, readParams:ReadParams, writeParams:WriteParams, amount:BN): Promise<string> {

	const wallet = await unlockWallet(walletParams.provider, walletParams.passphrase);
	const walletAddress = wallet.getAddressString();

	// check balance
	const tokenContract = new w3.eth.Contract(abi['common']['erc20'], readParams.sourceTokenAddress);
	const tokenSymbol = await tokenContract.methods.symbol().call();
	const tokenBalance = await tokenContract.methods.balanceOf(walletAddress).call();
	if (amount.gt(Web3.utils.toBN(tokenBalance))) {
		throw 'Insufficient ' + tokenSymbol + ' balance. Wanted ' + amount + ', have ' + tokenBalance + '\n';
	}
	const gasBalance = await w3.eth.getBalance(walletAddress); //.getAddressString());
	console.debug('gas balance', walletAddress, gasBalance);
	console.debug('token balance', tokenSymbol, walletAddress, tokenBalance);

	const tx = await transfer(w3, wallet, readParams, writeParams, amount);
	await w3.eth.sendSignedTransaction(tx['raw']);
	return tx['hash'];
}



(async () => {
	let w3 = undefined;
	let r = 0;
	try {
		w3 = new Web3(config.get('ETH_PROVIDER'))

		const amount = Web3.utils.toBN(argv['_'][0]);

		const walletParams = {
			provider: config.get('WALLET_KEYSTORE_FILE'),
			typ: 'keystoreV3',
			passphrase: process.env['WALLET_PASSWORD'],
		}

		const walletAddress = getWalletAddress(config.get('WALLET_KEYSTORE_FILE'));
		const common = newChainCommon(config.get('ETH_CHAIN_NAME'), parseInt(config.get('ETH_CHAIN_ID'), 10));	
		const readParams = {
			sourceTokenAddress: Web3.utils.toChecksumAddress(argv['token-address']),
			holderAddress: walletAddress,
			chainInfo: common,
		}

		const nonce = await w3.eth.getTransactionCount(walletAddress, 'pending');
		const writeParams = {
			beneficiaryAddress: Web3.utils.toChecksumAddress(argv['recipient-address']),
			chainInfo: common,
			nonce: nonce,
		}
	
		const result = await main(w3, walletParams, readParams, writeParams, amount); 
		process.stdout.write(result + "\n");
	} catch(e) {
		r = 1;
		process.stderr.write('error: ' + e + '\n');
		try {
			w3.currentProvider.connection.close()
		} catch {
		}
	}
	try {
		w3.currentProvider.connection.close()
	} catch {
	}
	process.exit(r);
})();
