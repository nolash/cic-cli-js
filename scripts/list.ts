#!/usr/bin/env node

const { Console } = require('console');
console = new Console({
	stdout: process.stderr,
	stderr: process.stderr,
});

import * as path from 'path';

const args = require('yargs');

const Web3 = require('web3');

const xdgBasedir = require('xdg-basedir');

import Wallet from 'ethereumjs-wallet';

import { Transaction } from '@ethereumjs/tx';
import Common from '@ethereumjs/common';

import { Registry, abi } from 'cic-client';

import { standardArgs } from '../src/args';
import { loadRegistry } from '../src/registry';
import { loadConfig } from '../src/config';


let configPath = path.join(xdgBasedir.config, 'cic-cli');

const argv = standardArgs.usage(
	'ts-node list.ts [options]'
).argv;

if (argv['config'] !== undefined) {
	configPath = argv['config'];
}


const argvTranslation = {
	'keystore-file': 'WALLET_KEYSTORE_FILE',
	'registry-address': 'BANCOR_REGISTRY_ADDRESS',
	'provider': 'ETH_PROVIDER',
};
const config = loadConfig(configPath, argv, argvTranslation, process.env);
console.debug(config);


async function main(w3:any, registry:Registry) {
	await loadRegistry(registry, true);

	let tokens = [];
	for (let i = 0; i < registry.tokens.length; i++) {
		try {
			const t = registry.tokens[i];
			const o = {
				name: t.name,
				symbol: t.symbol,
				address: t.address,
				supply: t.totalSupply,
				decimals: t.decimals,
				reserves: {},
				reserveRatio: undefined,
				owner: undefined,
			};
			try {
				const c = registry.converters_t[t.address];
				c.reserves.forEach((rz) => {
					o.reserves[rz.address] = {
						weight: rz.weight,
						balance: rz.balance,
					};
				});
				o.owner = c.owner;
				o.reserveRatio = c.reserveRatio;
			} catch(e) {
				console.error('err', e);
			}
			//process.stdout.write(JSON.stringify(o) + "\n");
			tokens.push(o);
		} catch(e) {
			console.error('get token error ' + e);
			break;
		}
	}
	return tokens;
}

(async () => {
	let w3 = undefined;
	let r = 0;
	try {
		w3 = new Web3(config.get('ETH_PROVIDER'));
		const registry = new Registry(w3, config.get('BANCOR_REGISTRY_ADDRESS'), abi);

		const r = await main(w3, registry);
		process.stdout.write(JSON.stringify(r) + '\n');
	} catch(e) {
		r = 1;
		process.stderr.write('error: ' + e + '\n');
		try {
			w3.currentProvider.connection.close()
		} catch {
		}
	}

	try {
		w3.currentProvider.connection.close()
	} catch{
	}
	process.exit(r);
})();
