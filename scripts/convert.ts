#!/usr/bin/env node

const { Console } = require('console');
console = new Console({
	stdout: process.stderr,
	stderr: process.stderr,
});

import * as path from 'path';

const Web3 = require('web3');

const xdgBasedir = require('xdg-basedir');

import Wallet from 'ethereumjs-wallet';

import { BN } from 'bn';

import { Transaction } from '@ethereumjs/tx';
import Common from '@ethereumjs/common';

import { Registry, abi } from 'cic-client';

import { standardArgs } from '../src/args';
import { convert } from '../src/bancor';
import { loadRegistry } from '../src/registry';
import { unlockWallet } from '../src/wallet/keystore';
import { getWalletAddress } from '../src/wallet/keystore';
import { zeroAddress } from '../src/common';
import { loadConfig } from '../src/config';
import { pollFor } from '../src/transport';
import { newChainCommon, ReadParams, WriteParams, WalletParams } from '../src/param';

let configPath = path.join(xdgBasedir.config, 'cic-cli');


const argv = standardArgs.usage(
	'ts-node convert.ts [options] <source_token_amount>'
).option('keystore-file', {
	alias: 'f',
	type: 'string',
	description: 'Keystore file of account to deploy token with',

}).option('source-token-address', {
	type: 'string',
	description: 'Source token address',

}).option('destination-token-address', {
	type: 'string',
	description: 'Destination token address',

}).option('recipient-address', {
	alias: 'r',
	type: 'string',
	description: 'Recipient address (for now not implemented, recipient will always be wallet address)',

}).option('fee', {
	type: 'number',
	description: 'Conversion fee, in parts-per-million (10000 == 1%)',

}).option('fee-beneficiary', {
	type: 'number',
	description: 'Ethereum address fee should be sent to',

}).argv;


// Input validation for args
if (argv['config'] !== undefined) {
	configPath = argv['config'];
}
if (process.env['WALLET_PASSWORD'] === undefined) {
	throw 'please set the WALLET_PASSWORD environment variable to the keystore file password';
}

const argvTranslation = {
	'keystore-file': 'WALLET_KEYSTORE_FILE',
	'registry-address': 'BANCOR_REGISTRY_ADDRESS',
	'provider': 'ETH_PROVIDER',
	'fee': 'BANCOR_CONVERSION_FEE',
	'fee-beneficiary': 'BANCOR_CONVERSION_FEE_BENEFICIARY',
};
const config = loadConfig(configPath, argv, argvTranslation, process.env);
console.debug(config);


async function main(w3:any, registry:Registry, walletParams:WalletParams, readParams:ReadParams, writeParams:WriteParams, amount:BN): Promise<string> {
	await loadRegistry(registry, false);

	const wallet = await unlockWallet(walletParams.provider, walletParams.passphrase);
	const walletAddress = wallet.getAddressString();
	writeParams.beneficiaryAddress = walletAddress;

	const reserve = registry.reserves()[0];
	const reserveAddress = reserve._address;
	console.debug('reserve', reserveAddress);
	writeParams.anchorTokenAddress = reserveAddress;

	// check balance
	console.debug('convert', readParams.sourceTokenAddress, writeParams.destinationTokenAddress);
	const tokenContract = new w3.eth.Contract(registry.abis['common']['erc20'], readParams.sourceTokenAddress);
	const tokenSymbol = await tokenContract.methods.symbol().call();
	const tokenBalance = await tokenContract.methods.balanceOf(walletAddress).call();
	if (amount.gt(Web3.utils.toBN(tokenBalance))) {
		throw 'Insufficient ' + tokenSymbol + ' balance. Wanted ' + amount + ', have ' + tokenBalance + '\n';
	}
	const gasBalance = await w3.eth.getBalance(walletAddress); 
	console.debug('gas balance', walletAddress, gasBalance);
	console.debug('token balance', tokenSymbol, walletAddress, tokenBalance);

	const txs = await convert(w3, registry, wallet, readParams, writeParams, amount); 

	w3.eth.sendSignedTransaction(txs[0]['raw']);
	w3.eth.sendSignedTransaction(txs[1]['raw']);
	await w3.eth.sendSignedTransaction(txs[2]['raw']);
	return txs[2]['hash'];
}

(async () => {
	let w3 = undefined;
	let r = 1;
	try {
		w3 = new Web3(config.get('ETH_PROVIDER'))
		const registry = new Registry(w3, config.get('BANCOR_REGISTRY_ADDRESS'), abi);
		
		const amount = Web3.utils.toBN(argv['_'][0]);

		const walletParams = {
			provider: config.get('WALLET_KEYSTORE_FILE'),
			typ: 'keystoreV3',
			passphrase: process.env['WALLET_PASSWORD'],
		}
		const walletAddress = getWalletAddress(config.get('WALLET_KEYSTORE_FILE'));

		const common = newChainCommon(config.get('ETH_CHAIN_NAME'), parseInt(config.get('ETH_CHAIN_ID'), 10));	
		const readParams = {
			sourceTokenAddress: Web3.utils.toChecksumAddress(argv['source-token-address']),
			holderAddress: walletAddress,
			chainInfo: common,
		}

		const nonce = await w3.eth.getTransactionCount(walletAddress, 'pending');
		const writeParams = {
			destinationTokenAddress: Web3.utils.toChecksumAddress(argv['destination-token-address']),
			fee: Web3.utils.toBN(config.get('BANCOR_CONVERSION_FEE')),
			feeAddress: zeroAddress,
			nonce: nonce,
		}
	
		if (writeParams.fee > 0) {
			writeParams.feeAddress = Web3.utils.toChecksumAddress(config.get('BANCOR_CONVERSION_FEE_BENEFICIARY'));
			console.debug('fee', writeParams.fee, writeParams.feeAddress);
		}

		const result = await main(w3, registry, walletParams, readParams, writeParams, amount); 
		process.stdout.write(result + '\n');

	} catch(e) {
		r = 1;
		process.stderr.write('error: ' + e + '\n');
		try {
			w3.currentProvider.connection.close()
		} catch {
		}
	}
	try {
		w3.currentProvider.connection.close()
	} catch {
	}
	process.exit(r);
})();
