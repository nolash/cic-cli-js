#!/usr/bin/env node

const { Console } = require('console');
console = new Console({
	stdout: process.stderr,
	stderr: process.stderr,
});

import * as fs from 'fs';
import * as path from 'path';

const args = require('yargs');

const Web3 = require('web3');

const xdgBasedir = require('xdg-basedir');

import Wallet from 'ethereumjs-wallet';

import { Transaction } from '@ethereumjs/tx';
import Common from '@ethereumjs/common';

import { Registry, abi } from 'cic-client';

import { standardArgs } from '../src/args';
import { loadRegistry } from '../src/registry';
import { getWalletAddress } from '../src/wallet/keystore';
import { ReadParams } from '../src/param';
import { loadConfig } from '../src/config';

let configPath = path.join(xdgBasedir.config, 'cic-cli');


const argv = standardArgs.usage(
	'ts-node balance.ts [options]'
).option('keystore-file', {
	alias: 'f',
	type: 'string',
	description: 'Keystore file of account to deploy token with',

}).option('token-address', {
	alias: 't',
	type: 'string',
	description: 'Token address',

}).option('holder-address', {
	alias: 'a',
	type: 'string',
	description: 'Address to query balance for (overrides keystore file)',

}).argv;


// Input validation for args
if (argv['config'] !== undefined) {
	configPath = argv['config'];
}
if (argv['keystore-file'] !== undefined && argv['holder-address'] !== undefined) {
	process.stderr.write('Can only specify one of -f and -a');
	process.exit(1);
}


const argvTranslation = {
	'keystore-file': 'WALLET_KEYSTORE_FILE',
	'registry-address': 'BANCOR_REGISTRY_ADDRESS',
	'provider': 'ETH_PROVIDER',
};
const config = loadConfig(configPath, argv, argvTranslation, process.env);
console.debug(config);


async function main(w3:any, registry:Registry, readParams:ReadParams) {
	await loadRegistry(registry, true);

	const walletAddress = readParams.holderAddress;
	const reserve = registry.reserves()[0];
	const reserveAddress = reserve._address;
	console.debug('reserve', reserveAddress);

	const gasBalance = await w3.eth.getBalance(walletAddress);
	console.debug('gas balance', gasBalance);

	const tokenAddress = readParams.sourceTokenAddress;
	console.log('type', typeof readParams.sourceTokenAddress);

	let balances = [];
	let tokenList = undefined;
	if (tokenAddress !== undefined) {
		tokenList = [tokenAddress];
	} else {
		tokenList = registry.tokens;	
	}
	for (let i = 0; i <tokenList.length; i++) {
		const t = registry.tokens[i];
		const tokenContract = new w3.eth.Contract(registry.abis['common']['erc20'], t.address);
		try {
			const tokenBalance = await tokenContract.methods.balanceOf(walletAddress).call();
			const o = {
				name: t.name,
				symbol: t.symbol,
				address: t.address,
				decimals: t.decimals,
				totalSupply: t.totalSupply,
				balance: tokenBalance,
			}
			balances.push(o);
		} catch(e) {
			console.error('failed to get balance for token ' + t.symbol + ': ' + e);
		}
	}
	return balances;
}

(async () => {
	let w3 = undefined;
	let r = 0;
	try {
		w3 = new Web3(config.get('ETH_PROVIDER'))
		const registry = new Registry(w3, config.get('BANCOR_REGISTRY_ADDRESS'), abi);

		const readParams:ReadParams = {
			holderAddress: undefined,
			sourceTokenAddress: undefined,
		}

		if (argv['holder-address'] !== undefined) {
			readParams.holderAddress = Web3.utils.toChecksumAddress(argv['holder-address']);
		} else {
			readParams.holderAddress = getWalletAddress(config.get('WALLET_KEYSTORE_FILE'));
			console.debug('reading address from keystore file');
		}
		console.log('balances for', readParams.holderAddress);

		if (argv['token-address'] !== undefined) {
			readParams['sourceTokenAddress'] = Web3.utils.toChecksumAddress(argv['token-address']);
		}

		const r = await main(w3, registry, readParams);
		process.stdout.write(JSON.stringify(r) + '\n');
	} catch(e) {
		r = 1;
		process.stderr.write('error: ' + e + '\n');
		try {
			w3.currentProvider.connection.close()
		} catch {
		}
	}

	try {
		w3.currentProvider.connection.close()
	} catch{
	}
	process.exit(r);
})();
