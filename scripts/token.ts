#!/usr/bin/env node

const { Console } = require('console');
console = new Console({
	stdout: process.stderr,
	stderr: process.stderr,
});

import * as path from 'path';

const Web3 = require('web3');

const xdgBasedir = require('xdg-basedir');

import { Transaction } from '@ethereumjs/tx';

import { Registry, abi } from 'cic-client';

import { BN } from 'bn';
import { standardArgs } from '../src/args';
import { createConverter, mint, acceptConverterOwnership } from '../src/bancor';
import { loadRegistry } from '../src/registry';
import { getWalletAddress } from '../src/wallet/keystore';
import { unlockWallet } from '../src/wallet/keystore';
import { loadConfig } from '../src/config';
import { newChainCommon, ReadParams, WriteParams, WalletParams } from '../src/param';

let configPath = path.join(xdgBasedir.config, 'cic-cli');


const argv = standardArgs.usage(
	'ts-node token.ts [options] <reserve-amount>'
).option('keystore-file', {
	alias: 'f',
	type: 'string',
	description: 'Keystore file of account to deploy token with',

}).option('token-name', {
	alias: 'n',
	type: 'string',
	description: 'Token name',

}).option('token-symbol', {
	alias: 's',
	type: 'string',
	description: 'Token symbol',

}).option('token-decimals', {
	type: 'number',
	description: 'Token decimal resolution',
	default: 18,

}).option('max-fee', {
	type: 'string',
	default: 10000, // 1%
	description: 'Maximum conversion fee of converter, in parts per million',

}).option('reserve-weight', {
	alias: 'w',
	type: 'string',
	default: 1000000, // 100%
	description: 'Weight ratio between reserve and token, in parts per million',

}).argv;


// Input validation for args
if (argv['config'] !== undefined) {
	configPath = argv['config'];
}
if (process.env['WALLET_PASSWORD'] === undefined) {
	throw 'please set the WALLET_PASSWORD environment variable to the keystore file password';
}

const argvTranslation = {
	'keystore-file': 'WALLET_KEYSTORE_FILE',
	'registry-address': 'BANCOR_REGISTRY_ADDRESS',
	'provider': 'ETH_PROVIDER',
};
const config = loadConfig(configPath, argv, argvTranslation, process.env);
console.debug(config);


async function main(w3:any, registry:Registry, walletParams:WalletParams, readParams:ReadParams, writeParams:WriteParams, amount:BN): Promise<string> {
	await loadRegistry(registry);

	const wallet = await unlockWallet(walletParams['provider'], walletParams['passphrase']);
	const walletAddress = wallet.getAddressString();
	writeParams.beneficiaryAddress = walletAddress;

	let reserveToken = undefined;
	let reserveAddress = undefined;
	if (!readParams.sourceTokenAddress) {
		console.debug('reserve token not set, using default reserve');
		reserveToken = registry.reserves()[0];
		reserveAddress = reserveToken._address;
		readParams.sourceTokenAddress = reserveAddress;
	} else {
		reserveToken = new w3.eth.Contract(registry.abis['common']['erc20'], readParams.sourceTokenAddress);
		reserveAddress = readParams.sourceTokenAddress
	}
	console.debug('reserve', reserveAddress);

	// check balance
	const reserveBalance = await reserveToken.methods.balanceOf(walletAddress).call();
	if (reserveBalance < amount) {
		w3.currentProvider.connection.close()
		throw 'Insufficient balance. Wanted ' + amount + ', have ' + reserveBalance + '\n';
	}
	console.debug('reserve balance', walletAddress, reserveBalance);

	writeParams.destinationTokenAddress = await createConverter(w3, registry, wallet, readParams, writeParams);

	// TODO: this inline resolving of the converter for token somewhat breaks the pattern of concealing contract specifics to library
	console.log('abi', abi['common']['erc20']);
	const token = new w3.eth.Contract(abi['common']['erc20'], writeParams.destinationTokenAddress);
	const converterAddress = await token.methods.owner().call();
	const writeParamsAcceptOwnership = {
		...writeParams,
		nonce: ++writeParams.nonce,
		targetAddress: converterAddress,
	}
	acceptConverterOwnership(w3, registry, wallet, readParams, writeParamsAcceptOwnership);
	
	writeParams.nonce++;

	const txs = await mint(w3, registry, wallet, readParams, writeParams, amount);
	w3.eth.sendSignedTransaction(txs[0]['raw']);
	w3.eth.sendSignedTransaction(txs[1]['raw']);
	await w3.eth.sendSignedTransaction(txs[2]['raw']);

	return writeParams.destinationTokenAddress;
}


(async () => {
	let w3 = undefined;
	try {
		w3 = new Web3(config.get('ETH_PROVIDER'))
		const registry = new Registry(w3, config.get('BANCOR_REGISTRY_ADDRESS'), abi);

		const amount = Web3.utils.toBN(argv['_'][0]);

		const walletParams:WalletParams = {
			provider: config.get('WALLET_KEYSTORE_FILE'),
			typ: 'keystoreV3',
			passphrase: process.env['WALLET_PASSWORD'],
		}
		const walletAddress = getWalletAddress(config.get('WALLET_KEYSTORE_FILE'));

		const common = newChainCommon(config.get('ETH_CHAIN_NAME'), parseInt(config.get('ETH_CHAIN_ID'), 10));	
		const readParams:ReadParams = {
			holderAddress: getWalletAddress(config.get('WALLET_KEYSTORE_FILE')),
			chainInfo: common,
		};

		const nonce = await w3.eth.getTransactionCount(walletAddress, 'pending');
		const writeParams:WriteParams = {
			tokenName: argv['token-name'],
			tokenSymbol: argv['token-symbol'],
			tokenDecimals: argv['token-decimals'],
			tokenConversionMaxFee: argv['max-fee'],
			tokenReserveWeight: argv['reserve-weight'],
			nonce: nonce,
		}

		const result = await main(w3, registry, walletParams, readParams, writeParams, amount);
		process.stdout.write(result + '\n');
		try {
			w3.currentProvider.connection.close()
		} catch {
		}
	} catch(e) {
		process.stderr.write('error: ' + e);
		try {
			w3.currentProvider.connection.close()
		} catch {
		}
		process.exit(1);
	}
})();
