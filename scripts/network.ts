#!/usr/bin/env node

import * as fs from 'fs';
import * as path from 'path';

const Web3 = require('web3');

const xdgBasedir = require('xdg-basedir');

import Wallet from 'ethereumjs-wallet';

import { Transaction } from '@ethereumjs/tx';
import Common from '@ethereumjs/common';

import { Registry, abi } from 'cic-client';

import { standardArgs } from '../src/args';
import { createTx } from '../src/tx';
import { loadRegistry } from '../src/registry';
import { getWalletAddress } from '../src/wallet/keystore';
import { unlockWallet } from '../src/wallet/keystore';
import { loadConfig } from '../src/config';
import { transfer } from '../src/erc20';
import { pollFor } from '../src/transport';
import { newChainCommon, ReadParams, WriteParams, WalletParams } from '../src/param';


let configPath = path.join(xdgBasedir.config, 'cic-cli');

const argv = standardArgs.usage(
	'ts-node transfer.ts [options] <amount>'
).option('keystore-file', {
	alias: 'f',
	type: 'string',
	description: 'Keystore file of account to deploy token with',

}).option('reserve-token-address', {
	alias: 'r',
	type: 'string',
	description: 'Keystore file of account to deploy token with',

}).option('bancor-dir', {
	type: 'string',
	description: 'Absolute path to the bancor contracts-solidity repository root',

}).argv

// Input validation for args
if (argv['config'] !== undefined) {
	configPath = argv['config'];
}
if (process.env['WALLET_PASSWORD'] === undefined) {
	throw 'please set the WALLET_PASSWORD environment variable to the keystore file password';
}

const argvTranslation = {
	'keystore-file': 'WALLET_KEYSTORE_FILE',
	'eth-provider': 'ETH_PROVIDER',
};
const config = loadConfig(configPath, argv, argvTranslation, process.env);
console.debug(config);


async function deployContract(w3:any, wallet:Wallet, readParams:ReadParams, writeParams:WriteParams, abi:string, bytecode:string, args:Array<any>): Promise<any> {
	let c = new w3.eth.Contract(abi);
	writeParams.txData = c.deploy({
		data: bytecode,
		arguments: args,
	}).encodeABI();

	return await createTx(w3, wallet, readParams, writeParams);
}


async function sendAndRegister(w3:any, wallet:Wallet, readParams:ReadParams, writeParams:WriteParams, contractRegistryContract:any, tx:object, contractId:string) {
	//console.log('wrtiparm', writeParams);
	const rcpt = await w3.eth.sendSignedTransaction(tx);

	let writeParamsContract = {
		...writeParams,
		nonce: ++writeParams.nonce,
		targetAddress: contractRegistryContract._address,
	};

	console.log('register ' + Web3.utils.hexToAscii(contractId), rcpt['contractAddress']);
	writeParamsContract.txData = contractRegistryContract.methods.registerAddress(contractId, rcpt['contractAddress']).encodeABI();
	const contractIdTx = await createTx(w3, wallet, readParams, writeParamsContract);
	w3.eth.sendSignedTransaction(contractIdTx);
}

async function fullDeployContract(w3:any, wallet:Wallet, readParams:ReadParams, writeParams:WriteParams, contractRegistryContract:any, meta:object, contractId:string, param:any): Promise<object> {
	const writeParamsLocal = {
		...writeParams,
		nonce: ++writeParams.nonce,
	};
	const tx = await deployContract(w3, wallet, readParams, writeParamsLocal, meta['abi'], meta['bytecode'], param); //[contractRegistryAddress]);
	console.log('tx hash before', Web3.utils.hexToAscii(contractId), Web3.utils.keccak256(tx), writeParams.nonce, writeParamsLocal.nonce);
	await sendAndRegister(w3, wallet, readParams, writeParamsLocal, contractRegistryContract, tx, contractId);
	console.log('tx hash after', Web3.utils.hexToAscii(contractId), Web3.utils.keccak256(tx), writeParams.nonce, writeParamsLocal.nonce);
	return tx;
}

async function main(w3:any, walletParams:WalletParams, reserveTokenAddress:string, bancorDir:string, readParams:ReadParams, writeParams:WriteParams, contractMeta:object): Promise<string> {
	const wallet = await unlockWallet(walletParams.provider, walletParams.passphrase);
	const walletAddress = wallet.getAddressString();

	const gasBalance = await w3.eth.getBalance(walletAddress); 
	console.debug('gas balance', walletAddress, gasBalance);


	console.log('Deploy contract registry');
	let meta = contractMeta['ContractRegistry'];
	let tx = await deployContract(w3, wallet, readParams, writeParams, meta.abi, meta.bytecode, []);
	let rcpt = await w3.eth.sendSignedTransaction(tx);
	const contractRegistry = new w3.eth.Contract(meta['abi'], rcpt['contractAddress']);
	const contractRegistryAddress = contractRegistry._address;

	console.log('Register contract registry');
	const writeParamsContract = {
		...writeParams,
		nonce: ++writeParams.nonce,
	};
	writeParamsContract.targetAddress = contractRegistryAddress;
	let contractId = Web3.utils.asciiToHex('ContractRegistry');
	writeParamsContract.txData = contractRegistry.methods.registerAddress(contractId, contractRegistryAddress).encodeABI();
	tx = await createTx(w3, wallet, readParams, writeParamsContract);
	w3.eth.sendSignedTransaction(tx);
	const contractRegistryTxHash = Web3.utils.keccak256(tx);

	console.log('Deploy converter registry');
	meta = contractMeta['ConverterRegistry'];
	contractId = Web3.utils.asciiToHex('BancorConverterRegistry');
	await fullDeployContract(w3, wallet, readParams, writeParams, contractRegistry, meta, contractId, [contractRegistryAddress]);
	console.log('nonce', writeParams.nonce);

	writeParams.nonce++;
	console.log('Deploy converter registry data');
	meta = contractMeta['ConverterRegistryData'];
	contractId = Web3.utils.asciiToHex('BancorConverterRegistryData');
	await fullDeployContract(w3, wallet, readParams, writeParams, contractRegistry, meta, contractId, [contractRegistryAddress]);
	console.log('nonce', writeParams.nonce);
	
	writeParams.nonce++;
	console.log('Deploy Bancor network');
	meta = contractMeta['BancorNetwork'];
	contractId = Web3.utils.asciiToHex('BancorNetwork');
	await fullDeployContract(w3, wallet, readParams, writeParams, contractRegistry, meta, contractId, [contractRegistryAddress]);
	console.log('nonce', writeParams.nonce);
	
	writeParams.nonce++;
	console.log('Deploy conversion path finder');
	meta = contractMeta['ConversionPathFinder'];
	contractId = Web3.utils.asciiToHex('ConversionPathFinder');
	await fullDeployContract(w3, wallet, readParams, writeParams, contractRegistry, meta, contractId, [contractRegistryAddress]);
	console.log('nonce', writeParams.nonce);

	writeParams.nonce++;
	console.log('Deploy bonding curve');
	meta = contractMeta['BancorFormula'];
	contractId = Web3.utils.asciiToHex('BancorFormula');
	tx = await fullDeployContract(w3, wallet, readParams, writeParams, contractRegistry, meta, contractId, []);
	const bancorFormulaTxHash = Web3.utils.keccak256(tx);
	console.log('nonce', writeParams.nonce);

	writeParams.nonce++;
	console.log('Deploy converter factory');
	meta = contractMeta['ConverterFactory'];
	contractId = Web3.utils.asciiToHex('ConverterFactory');
	tx = await fullDeployContract(w3, wallet, readParams, writeParams, contractRegistry, meta, contractId, []);
	const converterFactoryTxHash = Web3.utils.keccak256(tx);
	console.log('nonce', writeParams.nonce);

	writeParams.nonce++;
	const writeParamsLiquidTokenConverterFactory = {
		...writeParams,
		nonce: ++writeParams.nonce,
	};
	console.log('Deploy liquid token factory');
	meta = contractMeta['LiquidTokenConverterFactory'];
	tx = await deployContract(w3, wallet, readParams, writeParamsLiquidTokenConverterFactory, meta.abi, meta.bytecode, []);
	rcpt = await w3.eth.sendSignedTransaction(tx);
	const liquidTokenConverterFactoryAddress = rcpt['contractAddress'];

	const writeParamsBNTToken = {
		...writeParams,
		nonce: ++writeParams.nonce,
		targetAddress: contractRegistryAddress,
	};
	console.log('Register hub token');
	contractId = Web3.utils.asciiToHex('BNTToken');
	writeParamsBNTToken.txData = contractRegistry.methods.registerAddress(contractId, reserveTokenAddress).encodeABI();
	tx = await createTx(w3, wallet, readParams, writeParamsBNTToken);
	w3.eth.sendSignedTransaction(tx);

	console.log('Register liquid token factory', liquidTokenConverterFactoryAddress);
	rcpt = await pollFor(w3, converterFactoryTxHash);
	const converterFactoryAddress = rcpt['contractAddress'];
	const writeParamsConverterFactory = {
		...writeParams,
		nonce: ++writeParams.nonce,
		targetAddress: converterFactoryAddress,
	}
	meta = contractMeta['ConverterFactory'];
	const converterFactory = new w3.eth.Contract(meta['abi'], converterFactoryAddress);
	writeParamsConverterFactory.txData = converterFactory.methods.registerTypedConverterFactory(liquidTokenConverterFactoryAddress).encodeABI();
	tx = await createTx(w3, wallet, readParams, writeParamsConverterFactory);
	await w3.eth.sendSignedTransaction(tx);

	console.log('Initialize bonding curve');
	rcpt = await pollFor(w3, bancorFormulaTxHash);
	const bancorFormulaAddress = rcpt['contractAddress'];
	const writeParamsBancorFormula = {
		...writeParams,
		nonce: ++writeParams.nonce,
		targetAddress: bancorFormulaAddress,
	}
	meta = contractMeta['BancorFormula'];
	const bancorFormula = new w3.eth.Contract(meta['abi'], bancorFormulaAddress);
	writeParamsBancorFormula.txData = bancorFormula.methods.init().encodeABI();
	tx = await createTx(w3, wallet, readParams, writeParamsBancorFormula);
	await w3.eth.sendSignedTransaction(tx);

	
	return contractRegistryAddress;
}


(async () => {
	let w3 = undefined;
	let r = 0;
	try {
		w3 = new Web3(config.get('ETH_PROVIDER'))

		const walletParams = {
			provider: config.get('WALLET_KEYSTORE_FILE'),
			typ: 'keystoreV3',
			passphrase: process.env['WALLET_PASSWORD'],
		}

		const walletAddress = getWalletAddress(config.get('WALLET_KEYSTORE_FILE'));
		const common = newChainCommon(config.get('ETH_CHAIN_NAME'), parseInt(config.get('ETH_CHAIN_ID'), 10));	
		const readParams:ReadParams = {
			holderAddress: getWalletAddress(config.get('WALLET_KEYSTORE_FILE')),
			chainInfo: common,
		};

		const nonce = await w3.eth.getTransactionCount(walletAddress, 'pending');
		const writeParams = {
			chainInfo: common,
			nonce: nonce,
		}

		const reserveTokenAddress = argv['reserve-token-address'];

		const contracts = {
			'ContractRegistry': 'ContractRegistry',
			'ConverterRegistry': 'BancorConverterRegistry',
			'ConverterRegistryData': 'BancorConverterRegistryData',
			'ConverterFactory': 'ConverterFactory',
			'ConversionPathFinder': 'ConversionPathFinder',
			'BancorFormula': 'BancorFormula',
			'BancorNetwork': 'BancorNetwork',
			'LiquidTokenConverterFactory': undefined,
		};
		let contractMeta = {};

		const bancorDir = argv['bancor-dir'];
		const bancorBuildDir = path.join(bancorDir, 'solidity/build/contracts');
		Object.keys(contracts).forEach((k) => {
			const contractFile = path.join(bancorBuildDir, k + '.json');
			const contractMetaRaw = fs.readFileSync(contractFile, 'utf-8');
			const contractMetaFull = JSON.parse(contractMetaRaw);
			contractMeta[k] = {
				bytecode: contractMetaFull['bytecode'],
				abi: contractMetaFull['abi'],
				id: contracts[k],
			};
		});

		const result = await main(w3, walletParams, reserveTokenAddress, bancorDir, readParams, writeParams, contractMeta);
		process.stdout.write(result + "\n");
	} catch(e) {
		r = 1;
		process.stderr.write('error: ' + e + '\n');
		try {	
			w3.currentProvider.connection.close()
		} catch {
		}
	}
	try {
		w3.currentProvider.connection.close()
	} catch {
	}
	process.exit(r);
})();
