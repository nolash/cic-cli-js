# CIC cli tools


## State of this project

This tools is in very early development and not ready for public consumption.


## Installation

`npm install`

This will put the script files in `dist/scripts/`. They have to be run individually (see "Scripts" below).

`cic-cli` uses the registry abstraction from the `cic-client` library.


## Usage

Signing is currently only provided through keystore file.

A couple of methods to create keystore files:

* `geth acccount create` on your command line
* `geth account import <exising_private_key_in_hex_file>` on your command line
* https://www.myetherwallet.com/create-wallet

Password to unlock keystore file must be passed by environment variable `WALLET_PASSWORD`.


## Configuration

Some argument defaults are read from configuration file. The application will expect this to be in your xdg-basedir (most likely `~/.config/cic-cli`. Defaults for the examples data can be found in the `examples/` folder. Custom configuration file path can be set with `-c`.


## Scripts

### Network deployment

`node dist/scripts/network.js --help`


### Token deployment

`node dist/scripts/token.js --help`


### Transfer ERC20

`node dist/scripts/transfer.js --help`


### Convert on bonding curve

`node dist/scripts/convert.js --help`


### Token balances

`node dist/scripts/balance.js --help`


### List all tokens on network

`node dist/scripts/list.js --help`


### List contracts in network

`node dist/scripts/contracts.js --help`

